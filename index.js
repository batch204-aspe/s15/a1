console.log("Hello World");

/*
	1. Create variables to store to the following user details:

	-first name - String
	-last name - String
	-age - Number
	-hobbies - Array
	-work address - Object

		-The hobbies array should contain at least 3 hobbies as Strings.
		-The work address object should contain the following key-value pairs:

			houseNumber: <value>
			street: <value>
			city: <value>
			state: <value>

	Log the values of each variable to follow/mimic the output.

	Note:
		-Name your own variables but follow the conventions and best practice in naming variables.
		-You may add your own values but keep the variable names and values Safe For Work.
*/

	//Add your variables and console log for objective 1 here:

	let myFirstName = "Mark Joseph";
	let myLastName = "Aspe";
	let myAge = 28;

	console.log("First Name: " + myFirstName);
	console.log("Last Name: " + myLastName);
	console.log("Age: " + myAge);


	let myHobbies = ["Watching Anime", "Watching Movie", "Playing Table Tennis"];
	console.log("Hobbies: ")
	console.log(myHobbies);

	let myWorkAddress = {

		houseNumber: 5,
		street: "Block 11",
		city: "Quezon City",
		state: "Metro Manila"
	};
	console.log("Work Address: ")
	console.log(myWorkAddress);
	




/*			
	2. Debugging Practice - Identify and implement the best practices of creating and using variables 
	   by avoiding errors and debugging the following codes:

			-Log the values of each variable to follow/mimic the output.
*/	

	let capFullName = "Steve Rogers";
	console.log("My full name is " + capFullName);

	let age = 40;
	console.log("My current age is: " + age);
	
	let friends = ["Tony","Bruce","Thor","Natasha","Clint", "Nick"];
	console.log("My Friends are: ")
	console.log(friends);

	let profile = {

		username: "captain_america",
		fullName: "Steve Rogers",
		age: 40,
		isActive: false,

	};
	console.log("My Full Profile: ")
	console.log(profile);

	let capFriendName = "Bucky Barnes";
	console.log("My bestfriend is: " + capFriendName);

	const lastLocation = "Arctic Ocean";
	//lastLocation = "Atlantic Ocean";
	console.log("I was found frozen in: " + lastLocation);